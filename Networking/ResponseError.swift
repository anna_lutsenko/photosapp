//
//  ResponseError.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

enum ResponseError: Error {
    case dataIsEmpty
    case parsingFailed
    
    var localizedDescription: String {
        switch self {
        case .dataIsEmpty:
            return "Data is empty"
        case .parsingFailed:
            return "Something went wrong"
        }
    }
}
