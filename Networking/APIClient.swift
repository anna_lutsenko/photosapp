//
//  APIClient.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    var baseURL = APIConstants.URLs.baseURL
    private var method: HTTPMethod = .get
    
    private var defaultParameters: Parameters {
        return [APIConstants.Access.key: APIConstants.Access.value]
    }
    
    func get() -> Self {
        self.method = .get
        return self
    }
    
    func run<T: Decodable>(decode type: T.Type,
                           parameters: Parameters?,
                           completionHandler: @escaping (DataResponse<Any>) -> Void) {
        var param = defaultParameters
        if let additionalParam = parameters {
            param = param.merging(additionalParam) { (_, new) in new }
        }
        
        Alamofire.request(baseURL,
                          method: method,
                          parameters: param)
            .responseJSON(completionHandler: completionHandler)
    }
}

class GetPhotosAPIClient: APIClient {
    func photos() -> Self {
        self.baseURL += "photos"
        return self
    }
    func search() -> Self {
        self.baseURL += "search/"
        return self
    }
}
