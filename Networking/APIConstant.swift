//
//  APIConstant.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

struct APIConstants {
    private init() {}
    
    struct Access {
        static let key = "client_id"
        static let value = "4c9fbfbbd92c17a2e95081cec370b4511659666240eb4db9416c40c641ee843b"
        
    }
    
    struct URLs {
        static let baseURL = "https://api.unsplash.com/"
    }
    
    struct Keys {
        static let page = "page"
        static let perPage = "per_page"
        static let query = "query"
    }
}
