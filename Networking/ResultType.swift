//
//  ResultType.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

public typealias ResultCallback<T: Codable> = (ResultType<T>) -> Void

public enum ResultType<T: Codable> {
    case success(T)
    case failure(Error)
    
    var value: T? {
        guard case .success(let value) = self else {
            return nil
        }
        return value
    }
    
    var error: Error? {
        guard case .failure(let error) = self else {
            return nil
        }
        return error
    }
}
