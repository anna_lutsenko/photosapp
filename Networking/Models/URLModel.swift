//
//  URLModel.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

public struct URLModel: Codable {
    public let raw: String
    public let full: String
    public let regular: String
    public let small: String
    public let thumb: String
}
