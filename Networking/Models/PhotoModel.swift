//
//  PhotoModel.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

public struct PhotoSearchResult: Codable {
    public let results: [PhotoModel]
}

public struct PhotoModel: Codable {
    public let id: String
    public let color: String?
    public let description: String?
    public let urls: URLModel
    public let links: LinkModel
    public let likes: Int?
}
