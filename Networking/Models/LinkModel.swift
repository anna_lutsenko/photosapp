//
//  LinkModel.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

public struct LinkModel: Codable {
    let selfURL: String
    let htmlURL: String
    let download: String
    let downloadLocation: String
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        selfURL = try container.decode(String.self, forKey: .selfURL)
        htmlURL = try container.decode(String.self, forKey: .htmlURL)
        download = try container.decode(String.self, forKey: .download)
        downloadLocation = try container.decode(String.self, forKey: .downloadLocation)
    }
}

extension LinkModel {
    enum CodingKeys: String, CodingKey {
        case selfURL = "self"
        case htmlURL = "html"
        case download
        case downloadLocation = "download_location"
    }
}
