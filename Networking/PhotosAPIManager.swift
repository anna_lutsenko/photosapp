//
//  PhotosAPIManager.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import Alamofire

public class PhotosAPIManager {
    public init () { }
    
    private var currentPage: Int = 1
    
    func genericRequest<T: Codable>(
        decode type: T.Type,
        parameters: Parameters? = nil,
        apiClient: APIClient,
        completion: @escaping ResultCallback<T>) {
        
        apiClient.run(decode: type,
                      parameters: parameters) { (response) in
                        
                        switch response.result {
                        case .success(_):
                            do {
                                guard let data = response.data else { throw ResponseError.dataIsEmpty }
                                let object = try JSONDecoder().decode(type, from: data)
                                completion(.success(object))
                            } catch let error {
                                print("Parse locations error =  \(error)")
                                completion(.failure(error))
                            }
                            
                        case .failure(let error):
                            completion(.failure(error))
                        }
                        
        }
    }
    
    public func getPhotos(_ completion: @escaping ResultCallback<[PhotoModel]>) {
        let apiClient = GetPhotosAPIClient()
            .get()
            .photos()
        let param: Parameters = [APIConstants.Keys.perPage: 30,
                                 APIConstants.Keys.page: currentPage]
        
        genericRequest(decode: [PhotoModel].self,
                       parameters: param,
                       apiClient: apiClient,
                       completion: completion)
    }
    
    public func nextPage(completion: @escaping ResultCallback<[PhotoModel]>) {
        currentPage += 1
        getPhotos(completion)
    }
    
    public func searchPhotos(with text: String,
                             completion: @escaping ResultCallback<PhotoSearchResult>) {
        let param: Parameters = [APIConstants.Keys.query: text]
        let apiClient = GetPhotosAPIClient()
            .get()
            .search()
            .photos()
        genericRequest(decode: PhotoSearchResult.self,
                       parameters: param,
                       apiClient: apiClient,
                       completion: completion)
    }
    
}
