//
//  PhotoCollectionViewCell.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit
import Networking
import AlamofireImage

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var imageView: UIImageView!
    
    func configure(with model: PhotoCellViewModel) {
        guard let url = model.thumbURL else { return }
        imageView.af_setImage(withURL: url)
    }
}

struct PhotoCellViewModel {
    let thumbURL: URL?
    let regularURL: URL?
    
    init(model: PhotoModel) {
        self.thumbURL = URL(string: model.urls.thumb)
        self.regularURL = URL(string: model.urls.regular)
    }
}
