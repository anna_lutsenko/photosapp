//
//  PhotosViewController.swift
//  PhotoApp
//
//  Created by Anna on 5/12/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit
import Networking
import SVProgressHUD

class PhotosViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    @IBOutlet private weak var searchBar: UISearchBar!
    
    private lazy var apiManager = PhotosAPIManager()
    private var isLoadingList = false
    
    var photos: [PhotoModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPhotos()
    }
    
    // MARK: - Requests
    private func loadPhotos() {
        SVProgressHUD.show()
        apiManager.getPhotos { [weak self] result in
            SVProgressHUD.dismiss()
            switch result {
            case .success(let photos):
                self?.photos = photos
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func nextPage() {
        apiManager.nextPage { [weak self] result in
            self?.isLoadingList = false
            switch result {
            case .success(let photos):
                self?.photos += photos
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchPhotos(for text: String) {
        apiManager.searchPhotos(with: text) { [weak self] result in
            switch result {
            case .success(let photos):
                self?.photos = photos.results
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK: - Scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList) {
            isLoadingList = true
            nextPage()
        }
    }
}

extension PhotosViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let textToSearch = searchBar.text,
            textToSearch.count >= 3 else {
                (collectionView.collectionViewLayout as? CustomLayout)?.numberOfColumns = 3
            return
        }
        (collectionView.collectionViewLayout as? CustomLayout)?.numberOfColumns = 1
        collectionView.reloadData()
        fetchPhotos(for: textToSearch)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        (collectionView.collectionViewLayout as? CustomLayout)?.numberOfColumns = 3
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        loadPhotos()
    }
}

extension PhotosViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let photoURL = PhotoCellViewModel(model: photos[indexPath.row]).regularURL,
            let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: SelectetedPhotoViewController.self)) as? SelectetedPhotoViewController else { return }
        vc.photoURL = photoURL
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        let viewModel = PhotoCellViewModel(model: photos[indexPath.row])
        cell.configure(with: viewModel)
        return cell
    }
}
