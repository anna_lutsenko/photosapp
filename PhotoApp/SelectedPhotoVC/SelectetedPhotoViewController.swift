//
//  SelectetedPhotoViewController.swift
//  PhotoApp
//
//  Created by Anna on 5/13/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit
import AlamofireImage

class SelectetedPhotoViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    var photoURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = photoURL else { return }
        imageView.af_setImage(withURL: url)
    }
}
